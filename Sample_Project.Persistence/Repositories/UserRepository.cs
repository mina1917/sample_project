﻿using Microsoft.EntityFrameworkCore;
using Sample_Project.Application.Interfaces.IRepositories;
using Sample_Project.Domain.Entities.Estates;
using Sample_Project.Domain.Entities.Users;
using Sample_Project.Persistence.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_Project.Persistence.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly DataBaseContext _context;
        public UserRepository(DataBaseContext context)
        {
            _context = context;
        }
        public IQueryable<User> GetAll()
        {
            return(_context.Set<User>());
        }

        public User GetById(long Id)
        {
            return (_context.Set<User>().Find(Id));
        }
        public Role GetRoleById(long Id)
        {
            return (_context.Set<Role>().Find(Id));
        }
        public void Insert(User user)
        {
            _context.Set<User>().Add(user);
        }
        public void SaveChanges()
        {
            _context.SaveChanges();
        }
      
        public void Update(User user)
        {
            if (user == null)
                throw new ArgumentNullException("Entity is Null");
            
            _context.Entry(user).State = EntityState.Modified;
           
        }
    }
}
