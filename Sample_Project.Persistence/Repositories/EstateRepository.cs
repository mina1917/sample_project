﻿using Microsoft.EntityFrameworkCore;
using Sample_Project.Application.Interfaces.IRepositories;
using Sample_Project.Domain.Entities.Estates;
using Sample_Project.Persistence.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_Project.Persistence.Repositories
{
    public class EstateRepository : IEstateRepository
    {
        private readonly DataBaseContext _context;
        public EstateRepository(DataBaseContext context)
        {
            _context = context;
        }
        public IQueryable<Estate> GetAll()
        {
            return(_context.Set<Estate>());
        }

        public Estate GetById(long Id)
        {
            return (_context.Set<Estate>().Find(Id));
        }
        public void Insert(Estate estate)
        {
            _context.Set<Estate>().Add(estate);
        }
        public void SaveChanges()
        {
            _context.SaveChanges();
        }
      
        public void Update(Estate estate)
        {
            if (estate == null)
                throw new ArgumentNullException("Entity is Null");
            
            _context.Entry(estate).State = EntityState.Modified;
           
        }
    }
}
