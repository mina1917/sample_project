﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EndPoint.Web.ViewComponents
{
    public class GetSideBar : ViewComponent
    {
        public GetSideBar()
        {
        }


        public IViewComponentResult Invoke()
        {
            return View(viewName: "GetSideBar");
        }

    }
}
