﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EndPoint.Web.ViewComponents
{
    public class GetMenu:ViewComponent
    {
        public GetMenu()
        {
        }


        public IViewComponentResult Invoke()
        {
            return View(viewName: "GetMenu");
        }

    }
}
