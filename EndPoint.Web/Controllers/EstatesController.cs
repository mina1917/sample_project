﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore.Internal;
using Sample_Project.Application.Services.Estates.Queries.GetEstates;
using Sample_Project.Application.Services.Estates.Commands.CreateEstate;
using Sample_Project.Application.Services.Estates.Commands.DeleteEstate;
using Sample_Project.Application.Services.Estates.Commands.EditEstate;
using Sample_Project.Application.Services.Estates.Queries.GetEstate;
using Sample_Project.Common;
using EndPoint.Web.Utilities;

namespace EndPoint.Web.Controllers
{
    [Authorize(Roles = "Operator")]
    public class EstatesController : Controller
    {
        private readonly IGetEstatesService _getEstatesService;
        private readonly IGetEstateService _getEstateService;
        private readonly ICreateEstateService _registerUserService;
        private readonly IDeleteEstateService _removeUserService;
        private readonly IEditEstateService _editUserService;
        public EstatesController(IGetEstatesService getEstatesService,
              IGetEstateService getEstateService
            , ICreateEstateService registerUserService
            , IDeleteEstateService removeUserService
            , IEditEstateService editUserService)
        {
            _getEstateService = getEstateService;
            _getEstatesService = getEstatesService;
            _registerUserService = registerUserService;
            _removeUserService = removeUserService;
            _editUserService = editUserService;
        }
        public IActionResult Index(string searchkey, int page = 1)
        {
            
            return View(_getEstatesService.Execute(new RequestGetEstateDto
            {
                Page = page,
                SearchKey = searchkey,
            }));
        }

        [HttpGet]
        public IActionResult Create()
        {

            return View();
        }


        [HttpPost]
        public IActionResult Create(GetEstatesDto request)
        {
            request.CreatorUserId = ClaimUtility.GetUserId(HttpContext.User).Value;
            if (ModelState.IsValid)
            {
                return Json(_registerUserService.Execute(request));
            }
            return Json(new ResultDto
            {
                IsSuccess = false,
                Message = "تمامی اطلاعات ستاره دار را وارد نمایید",
            });
        }


        [HttpPost]
        public IActionResult Delete(long EstateId)
        {
            return Json(_removeUserService.Execute(EstateId));
        }

        [HttpGet]
        public IActionResult Edit(long Id)
        {
            return View(_getEstateService.Execute(Id));
        }

        [HttpPost]
        public IActionResult Edit(GetEstatesDto request)
        {
            request.EditorUserId = ClaimUtility.GetUserId(HttpContext.User).Value;
            return Json(_editUserService.Execute(request));
        }

    }
}
