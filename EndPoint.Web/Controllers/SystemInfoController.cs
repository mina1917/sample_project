﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sample_Project.Application.Services.Users.Commands.EditUser;
using Sample_Project.Application.Services.Users.Commands.DeleteUser;
using Sample_Project.Application.Services.Users.Commands.CreateUser;
using Sample_Project.Application.Services.Users.Commands.UserSatusChange;
using Sample_Project.Application.Services.Users.Queries.GetRoles;
using Sample_Project.Application.Services.Users.Queries.GetUsers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore.Internal;

namespace EndPoint.Web.Controllers
{
    [Authorize(Roles = "Operator")]
    public class SystemInfoController : Controller
    {
        public SystemInfoController()
        {
        }
        public IActionResult Index()
        {
            return View();
        }

    }
}
