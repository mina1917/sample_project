#pragma checksum "E:\me\Sample_Project\Sample_Project\EndPoint.Web\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c4672501a2798d3dc5b8c2e4f1bd892816b86f43"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "E:\me\Sample_Project\Sample_Project\EndPoint.Web\Views\_ViewImports.cshtml"
using EndPoint.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "E:\me\Sample_Project\Sample_Project\EndPoint.Web\Views\_ViewImports.cshtml"
using EndPoint.Web.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c4672501a2798d3dc5b8c2e4f1bd892816b86f43", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"787913d4de7401a869ba1a739981b1403a750f71", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "E:\me\Sample_Project\Sample_Project\EndPoint.Web\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "صفحه اصلی";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<div class=""row bench-marks"">
    <div class=""col-md-2"">
        <div class=""media btn-lg btn-danger"">
            <div class=""media-left"">
                <span class=""fa fa-code""></span>
            </div>
            <div class=""media-body"">
                <h4 class=""media-heading"">
                    <span class=""digit""></span>
                    Asp.Net Core
                </h4>
            </div>
        </div>
    </div>

    <div class=""col-md-2"">
        <div class=""media btn-lg btn-info"">
            <div class=""media-left"">
                <span class=""fa fa-info""></span>
            </div>
            <div class=""media-body"">
                <h4 class=""media-heading"">
                    <span class=""digit""></span>
                    EntityFrameWork Core
                </h4>
            </div>
        </div>
    </div>

    <div class=""col-md-2"">
        <div class=""media btn-lg btn-warning"">
            <div class=""media-left"">
                <span class=""");
            WriteLiteral(@"fa fa-info""></span>
            </div>
            <div class=""media-body"">
                <h4 class=""media-heading"">
                    <span class=""digit""></span>
                    Repository Pattern
                </h4>
            </div>

        </div>
    </div>
    <div class=""col-md-2"">
        <div class=""media btn-lg btn-info"">
            <div class=""media-left"">
                <span class=""fa fa-lightbulb-o""></span>
            </div>
            <div class=""media-body"">
                <h4 class=""media-heading"">
                    <span class=""digit""></span>
                    Clean Architecture
                </h4>
            </div>

        </div>
    </div>
    <div class=""col-md-2"">
        <div class=""media btn-lg btn-default"">
            <div class=""media-left"">
                <span class=""fa fa-database""></span>
            </div>
            <div class=""media-body"">
                <h4 class=""media-heading"">
                    <span class=""digi");
            WriteLiteral("t\"></span>\r\n                    SQL Server (Code First)\r\n                </h4>\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
