using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Sample_Project.Application.Interfaces.Contexts;
using Sample_Project.Application.Services.Estates.Commands.CreateEstate;
using Sample_Project.Application.Services.Users.Commands.CreateUser;
using Sample_Project.Application.Services.Estates.Commands.DeleteEstate;
using Sample_Project.Application.Services.Users.Commands.DeleteUser;
using Sample_Project.Application.Services.Estates.Commands.EditEstate;
using Sample_Project.Application.Services.Users.Commands.EditUser;
using Sample_Project.Application.Services.Users.Commands.UserLogin;
using Sample_Project.Application.Services.Users.Commands.UserSatusChange;
using Sample_Project.Application.Services.Estates.Queries.GetEstate;
using Sample_Project.Application.Services.Estates.Queries.GetEstates;
using Sample_Project.Application.Services.Users.Queries.GetRoles;
using Sample_Project.Application.Services.Users.Queries.GetUsers;
using Sample_Project.Application.Interfaces.IRepositories;
using Sample_Project.Persistence.Repositories;
using Sample_Project.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Sample_Project.Common;

namespace EndPoint.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddAuthorization(options =>
            {
                options.AddPolicy(UserRoles.Admin, policy => policy.RequireRole(UserRoles.Admin));
                options.AddPolicy(UserRoles.Operator, policy => policy.RequireRole(UserRoles.Operator));
            });

            services.AddAuthentication(options =>
            {
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            }).AddCookie(options =>
            {
                options.LoginPath = new PathString("/Account/Signin");
                options.ExpireTimeSpan = TimeSpan.FromMinutes(5.0);
                options.AccessDeniedPath = new PathString("/Account/Signin");
            });

            services.AddEntityFrameworkSqlServer().AddDbContext<DataBaseContext>(options =>
            {
                options.UseSqlServer(Configuration["ConnectionStrings:ApplicationDbContextConnection"]); //appsettings.json
            });
            services.AddControllersWithViews();

            services.AddScoped<IDataBaseContext, DataBaseContext>();

            //TODO : Facad Pattern
            services.AddScoped<IGetUsersService, GetUsersService>();
            services.AddScoped<IGetRolesService, GetRolesService>();
            services.AddScoped<ICreateUserService, CreateUserService>();
            services.AddScoped<IDeleteUserService, DeleteUserService>();
            services.AddScoped<IEditUserService, EditUserService>();
            services.AddScoped<IUserLoginService, UserLoginService>();
            services.AddScoped<IUserSatusChangeService, UserSatusChangeService>();


            services.AddScoped<IGetEstatesService, GetEstatesService>();
            services.AddScoped<ICreateEstateService, CreateEstateService>();
            services.AddScoped<IDeleteEstateService, DeleteEstateService>();
            services.AddScoped<IGetEstateService, GetEstateService>();
            services.AddScoped<IEditEstateService, EditEstateService>();

            //Repository Injection
            services.AddScoped<IEstateRepository, EstateRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
