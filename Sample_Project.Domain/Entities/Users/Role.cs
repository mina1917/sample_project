﻿using Sample_Project.Domain.Entities.Commons;
using System.Collections.Generic;

namespace Sample_Project.Domain.Entities.Users
{
    public class Role : BaseEntity
    {
        public string Name { get; set; }

        public ICollection<UserInRole> UserInRoles { get; set; }
    }
}
