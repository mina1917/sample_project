﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_Project.Domain.Entities.Commons
{

    public abstract class BaseEntity<TKey>  
    {
        public TKey Id { get; set; }
        public DateTime InsertTime { get; set; } = DateTime.Now;
        public long? CreatorUserId { get; set; }
        public DateTime? UpdateTime { get; set; }
        public long? EditorUserId { get; set; }
        public bool IsRemoved { get; set; } = false;
        public DateTime? RemoveTime { get; set; }
    }
    public abstract class BaseEntity : BaseEntity<long>
    {
    }
}
