﻿using Sample_Project.Domain.Entities.Commons;
using Sample_Project.Domain.Entities.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_Project.Domain.Entities.Estates
{
    public class Estate : BaseEntity
    {
        public string No { get; set; }
        public string Title { get; set; }
        public double AreaMeter { get; set; }
        public string Address { get; set; }
        public EstateType EstateType { get; set; }

        public virtual Owner Owner { get; set; }
        public long OwnerId { get; set; }

        public virtual User CreatorUser { get; set; }
        public virtual User EditorUser { get; set; }
    }
    public enum EstateType
    {
        [Display(Name = "شمالی")]
        North = 1,

        [Display(Name = "جنوبی")]
        South = 2
    }
}
