﻿using Sample_Project.Domain.Entities.Users;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Sample_Project.Domain.Entities.Estates;

namespace Sample_Project.Application.Interfaces.Contexts
{
    public interface IDataBaseContext
    {
        #region Users
        DbSet<User> Users { get; set; }
        DbSet<Role> Roles { get; set; }
        DbSet<UserInRole> UserInRoles { get; set; }

        #endregion

        #region Estates
        DbSet<Estate> Estates { get; set; }
        DbSet<Owner> Owners { get; set; }
        #endregion

        int SaveChanges(bool acceptAllChangesOnSuccess);
        int SaveChanges();
        Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = new CancellationToken());
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken());

    }
}
