﻿using Sample_Project.Domain.Entities.Estates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_Project.Application.Interfaces.IRepositories
{
    public interface IEstateRepository
    {
        Estate GetById(long Id);
        IQueryable<Estate> GetAll();
        void Insert(Estate estate);
        void Update(Estate estate);
        void SaveChanges();
    }
}
