﻿using Sample_Project.Domain.Entities.Estates;
using Sample_Project.Domain.Entities.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_Project.Application.Interfaces.IRepositories
{
    public interface IUserRepository
    {
        User GetById(long Id);
        Role GetRoleById(long Id);
        IQueryable<User> GetAll();
        void Insert(User user);
        void Update(User user);
        void SaveChanges();
    }
}
