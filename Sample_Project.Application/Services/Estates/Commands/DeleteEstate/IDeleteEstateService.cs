﻿using Sample_Project.Application.Interfaces.Contexts;
using Sample_Project.Application.Interfaces.IRepositories;
using Sample_Project.Common;
using System;

namespace Sample_Project.Application.Services.Estates.Commands.DeleteEstate
{
    public interface IDeleteEstateService
    {
        ResultDto Execute(long EstateId);
    }
    public class DeleteEstateService : IDeleteEstateService
    {
        private readonly IEstateRepository _estateRepository;

        public DeleteEstateService(IEstateRepository estateRepository)
        {
            _estateRepository = estateRepository;
        }

        public ResultDto Execute(long EstateId)
        {
            var user = _estateRepository.GetById(EstateId);
            if (user == null)
            {
                return new ResultDto
                {
                    IsSuccess = false,
                    Message = "ملک مورد نظر یافت نشد"
                };
            }
            user.RemoveTime = DateTime.Now;
            user.IsRemoved = true;
            _estateRepository.SaveChanges();
            return new ResultDto()
            {
                IsSuccess = true,
                Message = "اطلاعات ملک با موفقیت حذف شد"
            };
        }
    }
}
