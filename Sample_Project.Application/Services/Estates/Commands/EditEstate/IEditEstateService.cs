﻿using Microsoft.EntityFrameworkCore;
using Sample_Project.Application.Interfaces.IRepositories;
using Sample_Project.Application.Services.Estates.Queries.GetEstates;
using Sample_Project.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_Project.Application.Services.Estates.Commands.EditEstate
{
    public interface IEditEstateService
    {
        ResultDto Execute(GetEstatesDto request);
    }
    public class EditEstateService : IEditEstateService
    {
        private readonly IEstateRepository _estateRepository;

        public EditEstateService(IEstateRepository estateRepository)
        {
            _estateRepository = estateRepository;
        }
        public ResultDto Execute(GetEstatesDto request)
        {
            var estate = _estateRepository.GetAll().Where(x => x.Id == request.Id).Include(x => x.Owner).FirstOrDefault();
            if (estate == null)
            {
                return new ResultDto
                {
                    IsSuccess = false,
                    Message = "ملک مورد نظر یافت نشد"
                };
            }

            estate.EditorUserId = request.EditorUserId;
            estate.UpdateTime = DateTime.Now;
            estate.Title = request.Title;
            estate.Address = request.Address;
            estate.No = request.No;
            estate.AreaMeter = request.AreaMeter;

            estate.Owner.EditorUserId = request.EditorUserId;
            estate.Owner.UpdateTime = DateTime.Now;
            estate.Owner.FirstName = request.FirstName;
            estate.Owner.LastName = request.LastName;
            estate.Owner.Phone = request.Phone;

            _estateRepository.SaveChanges();

            return new ResultDto()
            {
                IsSuccess = true,
                Message = "ویرایش اطلاعات ملک با موفقیت انجام شد"
            };

        }
    }
}
