﻿using Sample_Project.Application.Interfaces.Contexts;
using Sample_Project.Application.Interfaces.IRepositories;
using Sample_Project.Application.Services.Estates.Queries.GetEstates;
using Sample_Project.Common;
using Sample_Project.Domain.Entities.Estates;
using System;

namespace Sample_Project.Application.Services.Estates.Commands.CreateEstate
{
    public interface ICreateEstateService
    {
        ResultDto Execute(GetEstatesDto request);
    }
    public class CreateEstateService : ICreateEstateService
    {
        private readonly IEstateRepository _estateRepository;

        public CreateEstateService(IEstateRepository estateRepository)
        {
            _estateRepository = estateRepository;
        }

        public ResultDto Execute(GetEstatesDto request)
        {
            Estate estate = new Estate()
            {
                No = request.No,
                Address = request.Address,
                Title = request.Title,
                EstateType = request.EstateType,
                CreatorUserId = request.CreatorUserId,
                AreaMeter = request.AreaMeter,
            };

            Owner owner = new Owner()
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Phone = request.Phone,
                CreatorUserId = request.CreatorUserId
            };
            estate.Owner = owner;
            _estateRepository.Insert(estate);
            _estateRepository.SaveChanges();

            return new ResultDto()
            {
                IsSuccess = true,
                Message = "اطلاعات ملک با موفقیت حذف شد"
            };
        }
    }
}
