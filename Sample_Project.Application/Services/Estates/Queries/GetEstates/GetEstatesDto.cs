﻿using Sample_Project.Domain.Entities.Commons;
using Sample_Project.Domain.Entities.Estates;
using System.Collections.Generic;

namespace Sample_Project.Application.Services.Estates.Queries.GetEstates
{
    public class GetEstatesDto : BaseEntity
    {
        public string No { get; set; }
        public string Title { get; set; }
        public string Address { get; set; }
        public double AreaMeter { get; set; }
        public EstateType EstateType { get; set; }
        public string CreatorUserName { get; set; }
        public string EditorUserName { get; set; }
        public long OwnerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
    }
}
