﻿using System.Collections.Generic;

namespace Sample_Project.Application.Services.Estates.Queries.GetEstates
{
    public class ReslutGetEstateDto
    {
        public int RowCount { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }

        public List<GetEstatesDto> Estates { get; set; }
        public int Rows { get; set; }
    }
}
