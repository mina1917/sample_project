﻿namespace Sample_Project.Application.Services.Estates.Queries.GetEstates
{
    public class RequestGetEstateDto
    {
        public string SearchKey { get; set; }
        public int Page { get; set; }
    }
}
