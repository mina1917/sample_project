﻿using Microsoft.EntityFrameworkCore;
using Sample_Project.Application.Interfaces.IRepositories;
using Sample_Project.Common;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;

namespace Sample_Project.Application.Services.Estates.Queries.GetEstates
{
    public interface IGetEstatesService
    {
        ReslutGetEstateDto Execute(RequestGetEstateDto request ,  int Page = 1);
    }
    public class GetEstatesService : IGetEstatesService
    {
        private readonly IEstateRepository _estateRepository;
        public GetEstatesService(IEstateRepository estateRepository)
        {
            _estateRepository = estateRepository;
        }

        public ReslutGetEstateDto Execute(RequestGetEstateDto request, int Page = 1)
        {
            var estates = _estateRepository.GetAll().Include(x => x.Owner).Include(x => x.CreatorUser).Include(x => x.EditorUser).AsQueryable();
            if (!string.IsNullOrWhiteSpace(request.SearchKey))
            {
                //TODO : Seperate Search Items
                estates = estates.Where(p => p.No.Contains(request.SearchKey) || p.Title.Contains(request.SearchKey) || p.Owner.FirstName.Contains(request.SearchKey) || p.Owner.LastName.Contains(request.SearchKey));
            }
            int rowsCount = 0;
            var estatesList = estates.ToPaged(request.Page, 20, out rowsCount).Select(p => new GetEstatesDto
            {
                No = p.No,
                Title = p.Title,
                Address = p.Address,
                FirstName = p.Owner.FirstName,
                LastName = p.Owner.LastName,
                EstateType = p.EstateType,
                Phone = p.Owner.Phone,
                CreatorUserName = p.CreatorUser != null ? p.CreatorUser.FullName : "",
                EditorUserName = p.EditorUser != null ? p.EditorUser.FullName : "",
                OwnerId = p.OwnerId,
                Id = p.Id,
                RemoveTime = p.RemoveTime,
                UpdateTime = p.UpdateTime,
                IsRemoved = p.IsRemoved
            }).ToList();
            int rowCount = 0;

            //TODO : Pagination in View 
            var result = new ReslutGetEstateDto
            {
                Estates = estatesList,
                CurrentPage = Page,
                PageSize = 20,
                RowCount = rowCount,
                Rows = rowsCount
            };

            return result;
        }
    }
}
