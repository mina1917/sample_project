﻿using Microsoft.EntityFrameworkCore;
using Sample_Project.Application.Interfaces.IRepositories;
using Sample_Project.Application.Services.Estates.Queries.GetEstates;
using Sample_Project.Common;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;

namespace Sample_Project.Application.Services.Estates.Queries.GetEstate
{
    public interface IGetEstateService
    {
        GetEstatesDto Execute(long Id);
    }
    public class GetEstateService : IGetEstateService
    {
        private readonly IEstateRepository _estateRepository;
        public GetEstateService(IEstateRepository estateRepository)
        {
            _estateRepository = estateRepository;
        }

        public GetEstatesDto Execute(long Id)
        {
            var model = _estateRepository.GetAll().Where(x => x.Id == Id).Include(x => x.Owner).FirstOrDefault();

            var result = new GetEstatesDto
            {
                Id = model.Id,
                OwnerId = model.OwnerId,
                Title = model.Title,
                Address = model.Address,
                AreaMeter = model.AreaMeter,
                EstateType = model.EstateType,
                InsertTime = model.InsertTime,
                CreatorUserId = model.CreatorUserId,
                No = model.No,
                FirstName = model.Owner.FirstName,
                Phone = model.Owner.Phone,
                LastName = model.Owner.LastName,
            };

            return result;
        }
    }
}
