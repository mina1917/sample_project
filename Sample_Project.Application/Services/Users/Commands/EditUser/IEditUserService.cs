﻿using Sample_Project.Application.Interfaces.IRepositories;
using Sample_Project.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_Project.Application.Services.Users.Commands.EditUser
{
    public interface IEditUserService
    {
        ResultDto Execute(RequestEditUserDto request);
    }
    public class EditUserService : IEditUserService
    {
        private readonly IUserRepository _userRepository;

        public EditUserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public ResultDto Execute(RequestEditUserDto request)
        {
            var user = _userRepository.GetById(request.UserId);
            if (user == null)
            {
                return new ResultDto
                {
                    IsSuccess = false,
                    Message = "کاربر یافت نشد"
                };
            }

            user.FullName = request.Fullname;
            _userRepository.SaveChanges();

            return new ResultDto()
            {
                IsSuccess = true,
                Message = "ویرایش کاربر انجام شد"
            };

        }
    }
}
