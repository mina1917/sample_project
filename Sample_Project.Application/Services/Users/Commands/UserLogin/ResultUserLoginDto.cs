﻿using System.Collections.Generic;

namespace Sample_Project.Application.Services.Users.Commands.UserLogin
{
    public class ResultUserLoginDto
    {
        public long UserId { get; set; }
        public List<string> Roles { get; set; }
        public string Name { get; set; }
    }
}
