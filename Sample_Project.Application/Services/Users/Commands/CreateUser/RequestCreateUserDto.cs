﻿using System.Collections.Generic;

namespace Sample_Project.Application.Services.Users.Commands.CreateUser
{
    public class RequestCreateUserDto
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string RePasword { get; set; }
        public List<RolesInCreateUserDto> Roles { get; set; }
    }


    public class RolesInCreateUserDto
    {
        public long Id { get; set; }
    }

    public class ResultCreateUserDto
    {
        public long UserId { get; set; }
    }

}
