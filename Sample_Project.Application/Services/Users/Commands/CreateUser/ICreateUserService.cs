﻿using Sample_Project.Common;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Sample_Project.Application.Interfaces.IRepositories;
using System.Text.RegularExpressions;
using Sample_Project.Domain.Entities.Users;
using System.Collections.Generic;
using System;

namespace Sample_Project.Application.Services.Users.Commands.CreateUser
{
    public interface ICreateUserService
    {
        ResultDto<ResultCreateUserDto> Execute(RequestCreateUserDto request);
    }
    public class CreateUserService : ICreateUserService
    {
        private readonly IUserRepository _userRepository;

        public CreateUserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public ResultDto<ResultCreateUserDto> Execute(RequestCreateUserDto request)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(request.Email))
                {
                    return new ResultDto<ResultCreateUserDto>()
                    {
                        Data = new ResultCreateUserDto()
                        {
                            UserId = 0,
                        },
                        IsSuccess = false,
                        Message = "پست الکترونیک را وارد نمایید"
                    };
                }

                if (string.IsNullOrWhiteSpace(request.FullName))
                {
                    return new ResultDto<ResultCreateUserDto>()
                    {
                        Data = new ResultCreateUserDto()
                        {
                            UserId = 0,
                        },
                        IsSuccess = false,
                        Message = "نام را وارد نمایید"
                    };
                }
                if (string.IsNullOrWhiteSpace(request.Password))
                {
                    return new ResultDto<ResultCreateUserDto>()
                    {
                        Data = new ResultCreateUserDto()
                        {
                            UserId = 0,
                        },
                        IsSuccess = false,
                        Message = "رمز عبور را وارد نمایید"
                    };
                }
                if (request.Password != request.RePasword)
                {
                    return new ResultDto<ResultCreateUserDto>()
                    {
                        Data = new ResultCreateUserDto()
                        {
                            UserId = 0,
                        },
                        IsSuccess = false,
                        Message = "رمز عبور و تکرار آن برابر نیست"
                    };
                }
                string emailRegex = @"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[A-Z0-9.-]+\.[A-Z]{2,}$";

                var match = Regex.Match(request.Email, emailRegex, RegexOptions.IgnoreCase);
                if (!match.Success)
                {
                    return new ResultDto<ResultCreateUserDto>()
                    {
                        Data = new ResultCreateUserDto()
                        {
                            UserId = 0,
                        },
                        IsSuccess = false,
                        Message = "ایمیل خودرا به درستی وارد نمایید"
                    };
                }

                var passwordHasher = new PasswordHasher();
                var hashedPassword = passwordHasher.HashPassword(request.Password);

                User user = new User()
                {
                    Email = request.Email,
                    FullName = request.FullName,
                    Password = hashedPassword,
                    IsActive = true,
                };

                List<UserInRole> userInRoles = new List<UserInRole>();

                foreach (var item in request.Roles)
                {
                    var roles = _userRepository.GetRoleById(item.Id);
                    userInRoles.Add(new UserInRole
                    {
                        Role = roles,
                        RoleId = roles.Id,
                        User = user,
                        UserId = user.Id,
                    });
                }
                user.UserInRoles = userInRoles;

                _userRepository.Insert(user);
                _userRepository.SaveChanges();

                return new ResultDto<ResultCreateUserDto>()
                {
                    Data = new ResultCreateUserDto()
                    {
                        UserId = user.Id,
                    },
                    IsSuccess = true,
                    Message = "ثبت نام کاربر با موفقیت انجام شد",
                };
            }
            catch (Exception)
            {
                return new ResultDto<ResultCreateUserDto>()
                {
                    Data = new ResultCreateUserDto()
                    {
                        UserId = 0,
                    },
                    IsSuccess = false,
                    Message = "ثبت نام انجام نشد !"
                };
            }
        }
    }

}
