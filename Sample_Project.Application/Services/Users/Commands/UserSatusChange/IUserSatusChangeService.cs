﻿using Sample_Project.Application.Interfaces.Contexts;
using Sample_Project.Application.Interfaces.IRepositories;
using Sample_Project.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_Project.Application.Services.Users.Commands.UserSatusChange
{
    public interface IUserSatusChangeService
    {
        ResultDto Execute(long UserId);
    }

    public class UserSatusChangeService : IUserSatusChangeService
    {
        private readonly IUserRepository _userRepository;


        public UserSatusChangeService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public ResultDto Execute(long UserId)
        {
            var user = _userRepository.GetById( UserId);
            if (user == null)
            {
                return new ResultDto
                {
                    IsSuccess = false,
                    Message = "کاربر یافت نشد"
                };
            }

            user.IsActive = !user.IsActive;
            _userRepository.SaveChanges();
            string userstate = user.IsActive == true ? "فعال" : "غیر فعال";
            return new ResultDto()
            {
                IsSuccess = true,
                Message = $"کاربر با موفقیت {userstate} شد!",
            };
        }
    }
}
